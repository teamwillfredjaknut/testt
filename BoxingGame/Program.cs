﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingGame
{

    namespace ConsoleApp1
    {


        class Boxer
        {
            public string Name { get; set; }
            public int Health { get; set; }
            public int Strength { get; set; }
            public int Stamina { get; set; }
            public int Victories { get; set; }
            public Weapon Weapon { get; set; }

        }

        public abstract class Weapon
        {
            public Weapon(int damage, int durability, double critChange, int weight, string name)
            {
                Damage = damage;
                Durability = durability;
                CritChange = critChange;
                Weight = weight;
                Name = name;
            }

            public int Damage { get; set; }
            public int Durability { get; set; }
            public double CritChange { get; set; }
            public int Weight { get; set; }
            public string Name { get; set; }



        }
        // hej 
        public class PeircingWeapon : Weapon
        {

            public PeircingWeapon(int damage, int durability, double critChange, int weight, string name) : base(damage, durability, critChange, weight, name)
            {

            }

            public void Bleed()
            {

            }
        }

        public class BludgingWeapon : Weapon
        {

            public BludgingWeapon(int damage, int durability, double critChange, int weight, string name) : base(damage, durability, critChange, weight, name)
            {

            }

        }

        public class SlashingWeapon : Weapon
        {
            public SlashingWeapon(int damage, int durability, double critChange, int weight, string name) : base(damage, durability, critChange, weight, name)
            {

            }
        }

        internal class BoxingGame
        {
            public const int StartingHealth = 20;
            // Jakobs Main Function
            static void Main(string[] args)
            {
                BoxingGame game = new BoxingGame();
                game.StartGame();
            }

            // Frederik 
            public void CheckIfRoundIsOver(List<Boxer> players)
            {
                if (players[0].Health <= 0)
                {
                    PrintRoundWinner(players[1], players[0]);

                }

                else if (players[1].Health <= 0)
                {
                    PrintRoundWinner(players[0], players[1]);
                }


            }

            // Frederik sds
            public bool CheckIfGameIsOver(List<Boxer> players, int matches)
            {
                foreach (Boxer player in players)
                {
                    if (player.Victories >= matches)
                    {
                        PrintMatchWinner(player);
                        return true;
                    }

                }
                return false;
            }

            // Williams funktion  
            public void UpdatePlayerHealth(List<Boxer> players, int healthAmount)
            {
                foreach (Boxer player in players)
                {
                    player.Health = healthAmount;
                }
            }

            // test test 
            public void PrintRoundWinner(Boxer WinnerPlayer, Boxer LoserPlayer)
            {
                Console.WriteLine("");
                WinnerPlayer.Victories++;
                Console.WriteLine();

                Console.WriteLine(WinnerPlayer.Name + " knocked down " + LoserPlayer.Name + ". " + WinnerPlayer.Name + " total number of knock downs " + WinnerPlayer.Victories);

                Console.WriteLine("=========================================================================");
                Console.WriteLine("############################## {0} Wins the round ##################################", WinnerPlayer.Name);
                Console.WriteLine("=========================================================================");
                Console.WriteLine();

                UpdatePlayerHealth(new List<Boxer> { WinnerPlayer, LoserPlayer }, StartingHealth);
            }

            public void PrintMatchWinner(Boxer WinnerPlayer)
            {
                Console.WriteLine();
                Console.WriteLine("=========================================================================");
                Console.WriteLine("*_ **_= ***_===****|| {0} is the Champion ****===_***==_**_*", WinnerPlayer.Name);
                Console.WriteLine("=========================================================================");
                Console.WriteLine();
            }

            // William  
            public int CalculateDmg(int strength, int crit)
            {
                Random rnd = new Random();
                int dmg = rnd.Next(1, strength + crit);
                return dmg;

            }

            public void FightSequence(Boxer player1, Boxer player2, int player1crit, int player2crit)
            {
                Attack(player1, player2, player1crit);
                Attack(player2, player1, player2crit);
                Console.WriteLine("");
            }

            public void Attack(Boxer attackingPlayer, Boxer receivingPlayer, int player1crit)
            {
                var dmg = CalculateDmg(attackingPlayer.Strength, player1crit);
                Console.WriteLine("{0} Hits {1} for {2}, {1} takes {2} Damage, {1} now has {3} health left", attackingPlayer.Name, receivingPlayer.Name, dmg, receivingPlayer.Health -= dmg);
            }

            public void RegainHealth(Boxer player)
            {
                Random rnd = new Random();
                var regen = rnd.Next(player.Stamina);
                Console.WriteLine("{0} Regains some of his stamina back and recives {1} health, now {0} has {2} health", player.Name, regen, player.Health += regen);
            }

            public Weapon ChooseWeapon(Boxer player)
            {
                List<Weapon> weaps = InitializeWeapons();

                Console.WriteLine("Choose one of these weapons");
                foreach (Weapon weap in weaps)
                {
                    Console.WriteLine("{0}: {1} ", weaps.IndexOf(weap) + 1, weap.Name);
                    Console.WriteLine("Damage: {0} ", weap.Damage);
                    Console.WriteLine("Durability: {0} ", weap.Durability);
                    Console.WriteLine("Weight: {0} ", weap.Weight);
                    Console.WriteLine("CritChange :{0} ", weap.CritChange);
                    Console.WriteLine("");

                }
                string weaponChosen = Console.ReadLine().ToString();
                int weapChose = Convert.ToInt32(weaponChosen);
                player.Weapon = weaps[weapChose - 1];
                return player.Weapon;
            }

            public List<Weapon> InitializeWeapons()
            {
                List<Weapon> weapList = new List<Weapon>();
                Weapon peicWeap = new PeircingWeapon(30, 5, 0.2, 10, "Knife");
                Weapon bludgingWeap = new BludgingWeapon(60, 10, 0.1, 30, "Club");
                Weapon slashingWeap = new SlashingWeapon(40, 7, 0.3, 20, "Sword");
                weapList.AddRange(new List<Weapon> { peicWeap, bludgingWeap, slashingWeap });

                return weapList;
            }


            public List<Boxer> InitializePlayers()
            {
                var player1 = new Boxer();
                var player2 = new Boxer();
                List<Boxer> boxers = new List<Boxer>();



                player1.Health = 20;
                player1.Strength = 10;
                player1.Victories = 0;
                player1.Stamina = 2;



                player2.Health = 200;
                player2.Strength = 5;
                player2.Victories = 0;
                player2.Stamina = 10;

                boxers.Add(player1);
                boxers.Add(player2);
                return boxers;
            }

            public void StartGame()
            {

                List<Boxer> boxers = InitializePlayers();
                foreach (Boxer player in boxers)
                {
                    Console.WriteLine("Enter your name player {0}", boxers.IndexOf(player) + 1);
                    player.Name = Console.ReadLine().ToString();
                    ChooseWeapon(player);
                }


                Console.WriteLine("How many rounds do you want to fight");

                int matches;
                matches = Convert.ToInt32(Console.ReadLine());


                for (int BoxingSimmatches = 1; BoxingSimmatches <= matches; BoxingSimmatches++)
                {

                    for (int round = 0; round < 10; round++)
                    {
                        if (boxers[0].Health <= 0 || boxers[1].Health <= 0)
                        {
                            Console.WriteLine("");
                            Console.WriteLine("================= Round" + round + " has ended get ready for the next round =================");
                            Console.WriteLine("");
                            break;
                        }


                        for (int i = 0; i < 20; i++)
                        {
                            if (boxers[0].Health <= 0 || boxers[1].Health <= 0)
                            {

                                break;
                            }

                            Console.WriteLine();

                            Console.WriteLine("Press = W = for a normal Attack");
                            Console.WriteLine("Press = D = for a Special Attack");
                            Console.WriteLine("Which attack do you want to perform");


                            var input = Console.ReadLine();

                            switch (input)
                            {
                                case "w":
                                    FightSequence(boxers[0], boxers[1], 4, 1);
                                    break;
                                case "d":
                                    FightSequence(boxers[0], boxers[1], 12, 1);
                                    break;
                                case "color":
                                    Console.BackgroundColor = ConsoleColor.Cyan;
                                    Console.ForegroundColor = ConsoleColor.Black;
                                    break;

                                default:
                                    Console.WriteLine("It was not the keys you where told to press");
                                    break;

                            }
                            // regain health
                            foreach (Boxer boxer in boxers)
                            {
                                RegainHealth(boxer);
                            }

                            CheckIfRoundIsOver(boxers);

                            bool gameOver = CheckIfGameIsOver(boxers, matches);
                            if (gameOver)
                            {
                                StartGame();
                            }


                        }

                    }
                }


            }
        }
    }
}